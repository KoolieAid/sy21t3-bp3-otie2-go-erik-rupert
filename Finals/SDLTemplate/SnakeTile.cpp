#include "SnakeTile.h"

SnakeTile::SnakeTile()
{
	width = 0;
	height = 0;
	texture = loadTexture("gfx/snake_tile.png");

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

}

int SnakeTile::getWidth()
{
	return width;
}

int SnakeTile::getHeight()
{
	return height;
}

int SnakeTile::getX()
{
	return x;
}

int SnakeTile::getY()
{
	return y;
}

void SnakeTile::setX(int _x)
{
	x = _x;
}

void SnakeTile::setY(int _y)
{
	y = _y;
}

int SnakeTile::getPreviousX()
{
	return previousX;
}

int SnakeTile::getPreviousY()
{
	return previousY;
}

void SnakeTile::setPreviousX(int _x)
{
	previousX = _x;
}

void SnakeTile::setPreviousY(int _y)
{
	previousY = _y;
}

void SnakeTile::start()
{

}

void SnakeTile::update()
{
	//std::cout << "ID: " << id << " x: " << x << std::endl;
	//std::cout << "ID: " << id << " y: " << y << std::endl;
}

void SnakeTile::draw()
{
	blit(texture, x, y);
}
