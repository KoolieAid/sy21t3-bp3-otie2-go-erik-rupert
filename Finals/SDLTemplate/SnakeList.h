#pragma once
#include "GameObject.h"
#include "common.h"
#include "SnakeTile.h"

struct Node
{
    Node(SnakeTile* tile)
    {
        this->tile = tile;
    }
    ~Node()
    {
        delete tile;
    }
    Node* previous = nullptr;
    Node* next = nullptr;
    SnakeTile* tile;
};

class SnakeList :
    public GameObject
{
public:
    SnakeList();
    ~SnakeList();

	void start() override;
	void update() override;
	void draw() override;

    int size();
    void addNew();

    Node* getHead();

private:
    Node* head;
    Node* last; //So i don't have to reiterate again lmao
    int directionX;
    int directionY;

    int x;
    int y;
    int width;
    int height;

};

