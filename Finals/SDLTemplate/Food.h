#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
class Food :
    public GameObject
{
public:
    void start() override;
    void update() override;
    void draw() override;

    void setX(int _x);
    void setY(int _y);

    int getX();
    int getY();

    int getWidth();
    int getHeight();
private:
    SDL_Texture* texture;
    int x;
    int y;
    int width;
    int height;
};

