#include "Food.h"
#include "GameScene.h"
#include "SnakeTile.h"
#include "util.h"

void Food::start()
{
	texture = loadTexture("gfx/apple.png");
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	this->setX((rand() % SCREEN_WIDTH / width) * width);
	this->setY((rand() % SCREEN_HEIGHT / height) * height);
}

void Food::update()
{

}

void Food::draw()
{
	blit(texture, x, y);
}

void Food::setX(int _x)
{
	x = _x;
}

void Food::setY(int _y)
{
	y = _y;
}

int Food::getX()
{
	return x;
}

int Food::getY()
{
	return y;
}

int Food::getWidth()
{
	return width;
}

int Food::getHeight()
{
	return height;
}
