#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "SnakeList.h"
#include "Food.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void update();
	void draw();
	
private:
	SnakeList* list;
	Food* food;

	int score;
	int highscore;
	
};

