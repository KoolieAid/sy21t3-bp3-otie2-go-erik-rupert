#include "GameScene.h"
#include "text.h"
#include "util.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
}

GameScene::~GameScene()
{

}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here
	initFonts();

	food = new Food;

	addGameObject(food);

	list = new SnakeList();
	addGameObject(list);

	score = 0;
	highscore = score;
}

void GameScene::update()
{
	Scene::update();

	//Food
	for (int i = 0; i < objects.size(); i++)
	{
		SnakeTile* tile = dynamic_cast<SnakeTile*>(objects.at(i));
		if (tile == nullptr) continue;

		int collision = checkCollision(food->getX(), food->getY(), food->getWidth(), food->getHeight(),
			tile->getX(), tile->getY(), tile->getWidth(), tile->getHeight());

		if (collision != 1) continue;
		
		score++;

		food->setX((rand() % SCREEN_WIDTH / food->getWidth()) * food->getWidth());
		food->setY((rand() % SCREEN_HEIGHT / food->getHeight()) * food->getHeight());

		list->addNew();

		break;
	}

	//Out of bounds
	for (int i = 0; i < objects.size(); i++)
	{
		SnakeTile* tile = dynamic_cast<SnakeTile*>(objects.at(i));
		if (tile == nullptr) continue;

		if (tile->getX() < 0 || tile->getY() < 0 || tile->getX() >= SCREEN_WIDTH || tile->getY() >= SCREEN_HEIGHT)
		{
			if (score > highscore) highscore = score;
			score = 0;
			delete list;
			list = new SnakeList;
			addGameObject(list);
		}

		break;
	}

	//Self collision
	SnakeTile* head = list->getHead()->tile;
	for (int i = 0; i < objects.size(); i++)
	{
		SnakeTile* tile = dynamic_cast<SnakeTile*>(objects.at(i));
		if (tile == head || tile == nullptr) continue;

		int collision = checkCollision(head->getX(), head->getY(), head->getWidth(), head->getHeight(),
			tile->getX(), tile->getY(), tile->getWidth(), tile->getHeight());

		if (collision != 1) continue;

		if (score > highscore) highscore = score;
		score = 0;
		delete list;
		list = new SnakeList;
		addGameObject(list);

	}
	
	SDL_Delay(1000 /(score + 7));
}

void GameScene::draw()
{
	Scene::draw();

	drawText(80, 10, 255, 255, 255, TEXT_CENTER, "SCORE: %01d", score);
	drawText(250, 10, 255, 255, 255, TEXT_CENTER, "HS: %01d", highscore);
	drawText(10, 560, 255, 0, 0, TEXT_LEFT, "FPS: %01d", score + 7);
}