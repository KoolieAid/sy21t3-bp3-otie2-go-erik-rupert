#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
class SnakeTile :
    public GameObject
{
public:
    SnakeTile();

    int getWidth();
    int getHeight();
    int getX();
    int getY();

    void setX(int _x);
    void setY(int _y);
    int getPreviousX();
    int getPreviousY();
    void setPreviousX(int _x);
    void setPreviousY(int _y);

    void start() override;
    void update() override;
    void draw() override;

    int id;

private:
    SDL_Texture* texture;

    int x;
    int y;
    int previousX;
    int previousY;
    int width;
    int height;

};

