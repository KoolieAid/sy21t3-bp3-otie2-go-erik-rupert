#include "SnakeList.h"
#include "Scene.h"

SnakeList::SnakeList()
{
	directionX = 0;
	directionY = -1;
}

SnakeList::~SnakeList()
{
	Node* current = head;
	Node* toDelete;
	while (current != nullptr)
	{
		toDelete = current;
		current = current->next;
		delete toDelete;
	}
}
/**
 * this is absolutely trash lmao
 * Use for previousX: replacement gets the x of the previous tile before it
 */
void SnakeList::start()
{
	x = 300;
	y = 300;
	//Copied from my old work linked lists
	Node* current = nullptr;
	Node* prev = nullptr;
	last = nullptr;

	//Head
	current = new Node(new SnakeTile());
	current->tile->setX(x);
	current->tile->setY(y);
	current->tile->setPreviousX(x);
	current->tile->setPreviousY(y);

	head = current;
	prev = current;
	getScene()->addGameObject(current->tile);
	head->tile->id = 0;

	//Anything after head
	for (int i = 0; i < 1; i++)
	{
		current = new Node(new SnakeTile());
		current->previous = prev;
		prev->next = current;
		
		current->tile->setPreviousX(current->previous->tile->getX());
		current->tile->setPreviousY(current->previous->tile->getY());
		current->tile->setX(prev->tile->getX() + prev->tile->getWidth() * directionX * -1);
		current->tile->setY(prev->tile->getY() + prev->tile->getHeight() * directionY * -1);
		
		getScene()->addGameObject(current->tile);

		current->tile->id = i + 1;

		prev = current;
	}

	last = current;
}

void SnakeList::update()
{
	
	if (app.keyboard[SDL_SCANCODE_UP])
	{
		if (directionX * -1 != 0 && directionY * -1 != -1)
		{
			directionX = 0;
			directionY = -1;
		}
	}
	if (app.keyboard[SDL_SCANCODE_DOWN])
	{
		if (directionX * -1 != 0 && directionY * -1 != 1)
		{
			directionX = 0;
			directionY = 1;
		}
	}
	if (app.keyboard[SDL_SCANCODE_LEFT])
	{
		if (directionX * -1 != -1 && directionY * -1 != 0)
		{
			directionX = -1;
			directionY = 0;
		}
	}
	if (app.keyboard[SDL_SCANCODE_RIGHT])
	{
		if (directionX * -1 != 1 && directionY * -1 != 0)
		{
			directionX = 1;
			directionY = 0;
		}
	}

	head->tile->setPreviousX(head->tile->getX());
	head->tile->setPreviousY(head->tile->getY());
	head->tile->setX(head->tile->getX() + (head->tile->getWidth() * directionX));
	head->tile->setY(head->tile->getY() + (head->tile->getHeight() * directionY));

	Node* current = head->next;
	
	while(current != nullptr)
	{
		SnakeTile* currentTile = current->tile;

		//int id = current->tile->id;
		//if (current == nullptr) std::cout << "current is null ID:" << id << std::endl;
		//if (current->next == nullptr) std::cout << "current's next is null ID: " << id << std::endl;
		//if (current->previous == nullptr) std::cout << "current's previous is null ID: " << id << std::endl;

		//Position
		current->tile->setPreviousX(current->tile->getX());
		current->tile->setPreviousY(current->tile->getY());
		currentTile->setX(current->previous->tile->getPreviousX());
		currentTile->setY(current->previous->tile->getPreviousY());

	
		current = current->next;
	}
}

void SnakeList::draw()
{

}

int SnakeList::size()
{
	Node* current = head;
	int count = 0;

	while (true)
	{
		count++;
		if (current->next == nullptr)
		{
			return count;
		}
		current = current->next;
	}
}

void SnakeList::addNew()
{
	int dx, dy;
	if (last->previous->tile->getX() == last->tile->getX())
	{
		dx = 0;
	}
	if (last->previous->tile->getX() > last->tile->getX())
	{
		dx = -1;
	}
	if (last->previous->tile->getX() < last->tile->getX())
	{
		dx = 1;
	}

	if (last->previous->tile->getY() == last->tile->getY())
	{
		dy = 0;
	}
	if (last->previous->tile->getY() > last->tile->getY())
	{
		dy = -1;
	}
	if (last->previous->tile->getY() < last->tile->getY())
	{
		dy = 1;
	}

	Node* toAdd = new Node(new SnakeTile());

	toAdd->previous = last;
	last->next = toAdd;

	toAdd->tile->setX(toAdd->previous->tile->getX() + last->tile->getWidth() * dx);
	toAdd->tile->setY(toAdd->previous->tile->getY() + last->tile->getHeight() * dy);
	toAdd->tile->setPreviousX(toAdd->tile->getX());
	toAdd->tile->setPreviousY(toAdd->tile->getY());

	last = toAdd;

	getScene()->addGameObject(toAdd->tile);

}

Node* SnakeList::getHead()
{
	return head;
}
