#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include <iostream>

class Enemy;

class Lightning final : public GameObject
{
public:
	Lightning();
	~Lightning();

	void start() override;
	void update() override;
	void draw() override;

	int getX();
	int getY();

	void setX(int _x);
	void setY(int _y);

	int getWidth();
	int getHeight();

private:
	SDL_Texture* texture;

	int x;
	int y;

	int width;
	int height;

	bool isFlipped;

	int damage;
};