#include "LightningPowerUp.h"
#include "Player.h"
#include "Scene.h"

LightningPowerUp::LightningPowerUp(int _x, int _y, float _activationTime)
	:	PowerUp(_x, _y, _activationTime)
{
	lightningObj = new Lightning();
}

LightningPowerUp::~LightningPowerUp()
{
	delete lightningObj;
}

void LightningPowerUp::activate()
{
	if (this->getCurrentActTime() != 0)
	{
		return;
	}
	this->setCurrentActTime(this->getActivationTime());

	// Remember to set sound
	this->getSoundChunk()->volume = 50;
	SoundManager::playSound(this->getSoundChunk());

	// start of logic
	lightningObj->setX((getSpawner()->getX() + getSpawner()->getWidth() / 2) - 5);
	lightningObj->setY((getSpawner()->getY() - 4 + getSpawner()->getHeight() / 2) - lightningObj->getHeight() - 20);

}

void LightningPowerUp::start()
{
	texture = loadTexture("gfx/lightningLogo.png");
	SDL_QueryTexture(texture, NULL, NULL, getWidthReference(), getHeightReference());

	// rember to set sound
	this->setSoundChunk(SoundManager::loadSound("sound/electricity.ogg"));

	this->getScene()->addGameObject(lightningObj);
}

void LightningPowerUp::update()
{
	// Activation time
	if (this->getCurrentActTime() > 0) {
		this->setCurrentActTime(this->getCurrentActTime() - 1);
	}

	// might remove since it doesnt move in x axi
	this->setX(this->getX() + 0);
	if (getSpawner() == NULL)
	{
		this->setY(this->getY() + 1 * this->getSpeed());
	}

	// Extra Logic
	if (!app.keyboard[SDL_SCANCODE_F])
	{
		lightningObj->setX(-100);
	}

	if (!isActive())
	{
		lightningObj->setX(-100);
	}
}

void LightningPowerUp::draw()
{
	if (getSpawner() != NULL) return;

	blit(texture, getX(), getY());
}