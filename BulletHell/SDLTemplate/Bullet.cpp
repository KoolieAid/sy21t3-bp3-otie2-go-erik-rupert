#include "Bullet.h"
#include "util.h"
#include "Player.h"


Bullet::Bullet(int _x, int _y, float _directionX, float _directionY, int _speed, Side _side)
{
	x = _x;
	y = _y;
	directionX = _directionX;
	directionY = _directionY;
	speed = _speed;
	airResistance = 0.2;

	side = _side;
}

Bullet::Bullet(int _x, int _y, float _directionX, float _directionY, int _speed, Player* _target)
{
	x = _x;
	y = _y;
	directionX = _directionX;
	directionY = _directionY;
	speed = _speed;
	airResistance = 0.2;
	
	target = _target;
}

void Bullet::start()
{
	if (side == Side::PLAYER_SIDE)
	{
		texture = loadTexture("gfx/playerBullet.png");
	}
	else
	{
		texture = loadTexture("gfx/alienBullet.png");
	}

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	// Delta time shit that i didnt use or forgor to use
	p = 0;

	damage = 10;
}

void Bullet::update()
{
	//p = 0;
	//n = SDL_GetPerformanceCounter();
	//double deltaTime = (double)((n - p) * 1000 / (double)SDL_GetPerformanceFrequency());
	//std::cout << deltaTime << std::endl;


	x += (directionX * speed);
	y += (directionY * speed);

	//float dx = -1;
	//float dy = 0;
	//
	//calcSlope(target->getX(), target->getY(), x, y, &dx, &dy);
	//
	//x += (dx * speed);
	//y += (dy * speed);
	//airResistance += 0.03;

}

void Bullet::draw()
{
	blit(texture, x, y, -90.0f);
}

float Bullet::getX()
{
	return x;
}

float Bullet::getY()
{
	return y;
}

int Bullet::getWidth()
{
	return width;
}

int Bullet::getHeight()
{
	return height;
}

Side Bullet::getSide()
{
	return side;
}

int Bullet::getDamage()
{
	return damage;
}
