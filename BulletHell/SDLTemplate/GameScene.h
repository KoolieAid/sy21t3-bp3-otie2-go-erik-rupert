#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include <vector>
#include "text.h"

#include "Gatling.h"
#include "LightningPowerUp.h"

class Boss;

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

	std::vector<GameObject*>* getGarbageBin();
	std::vector<Enemy*>* getEnemyVector();
	std::vector<GameObject*>* getObjectsVector();

	Boss* getBoss();

	void addPoint();
private:
	GameObject* player;

	// Spawning logic vars
	float spawnTime;
	float currentSpawnTimer;
	std::vector<Enemy*> spawnedEnemies;
	std::vector<PowerUp*> spawnedPowerUps;
	std::vector<GameObject*> garbageBin;

	void spawn();
	void despawn(Enemy* enemy);

	void spawnTimer();
	void collisionChecker();

	void outOfBoundsChecker();
	void spawnRandomPowerUp();

	void powerUpCollisionChecker();
	void powerUpOutOfBoundsChecker();

	void spawnBoss();

	void throwGarbageAway();
	void bossHpChecker();
	void deleteBoss(Boss* _boss);
	int points;

	Boss* boss;
};
