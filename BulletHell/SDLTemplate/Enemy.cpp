#include "Enemy.h"
#include "GameScene.h"
#include "util.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
	std::vector<GameObject*>* deleteVector = ((GameScene*)this->getScene())->getGarbageBin();
	int storedSize = spawnedBullets.size();
	for (int i = 0; i < storedSize; i++)
	{
		Bullet* toDelete = spawnedBullets[0];
		deleteVector->push_back(toDelete);
		spawnedBullets.erase(spawnedBullets.begin());
	}
}

void Enemy::start()
{
	texture = loadTexture("gfx/enemy.png");

	//x = (SCREEN_HEIGHT / 5) * 5;
	//y = SCREEN_HEIGHT / 2;
	reloadTime = 60;
	currentReloadTime = 0;
	speed = 3;
	directionX = -1;
	directionY = 1;
	directionChangeTime = (rand() % 200) + 100;
	currentDiractionChangeTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 60;
}

void Enemy::update()
{
	// Movement logic
	x += directionX * speed;
	y += directionY * speed;

	if (currentDiractionChangeTime > 0)
		currentDiractionChangeTime--;
	 
	if ((currentDiractionChangeTime == 0) || x + width > SCREEN_WIDTH || x <= 0) {
		directionX *= -1;
		currentDiractionChangeTime = directionChangeTime;
	}


	/// <summary>
	/// Firing logic. wtf is this summary feature
	/// </summary>
	if (currentReloadTime > 0) {
		currentReloadTime--;
	}

	if (currentReloadTime == 0) {
		
		float dx = -1;
		float dy = 0;

		calcSlope(target->getX(), target->getY(), x, y, &dx, &dy);

		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x - 3, y - 4 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);

		//Bullet* bullet = new Bullet(x - 3, y - 4 + height / 2, dx, dy, 10, target);
		spawnedBullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		currentReloadTime = reloadTime;
	}

	std::vector<GameObject*>* deleteVector = ((GameScene*)this->getScene())->getGarbageBin();

	for (int i = 0; i < spawnedBullets.size(); i++)
	{
		// If out of bounds
		if (spawnedBullets[i]->getX() < 0 || spawnedBullets[i]->getX() > SCREEN_WIDTH ||
			spawnedBullets[i]->getY() < 0 || spawnedBullets[i]->getY() > SCREEN_HEIGHT)
		{
			Bullet* toDelete = spawnedBullets[i];
			spawnedBullets.erase(spawnedBullets.begin() + i);
			//delete toDelete;
			
			deleteVector->push_back(toDelete);


			break;
		}
	}
}

void Enemy::draw()
{
	blit(texture, x, y, -90.0f);
}

void Enemy::setTarget(Player* _player)
{
	target = _player;
}

void Enemy::setPosition(int x, int y)
{
	this->x = x;
	this->y = y;
}

int Enemy::getWidth()
{
	return width;
}

int Enemy::getHeight()
{
	return height;
}

int Enemy::getX()
{
	return x;
}

int Enemy::getY()
{
	return y;
}
