#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>
#include "util.h"
#include "Player.h"
 
class Enemy :
    public GameObject 
{
public:
    Enemy();
    ~Enemy();
    void start() override;
    void update() override;
    void draw() override;

    //Sets the target player
    void setTarget(Player* _player);
    void setPosition(int x, int y);

    int getWidth();
    int getHeight();
    int getX();
    int getY();

private:
    SDL_Texture* texture;
    Mix_Chunk* sound;
    Player* target;
    float speed;
    int x;
    int y;
    float directionX;
    float directionY;
    float directionChangeTime;
    float currentDiractionChangeTime;
    int width;
    int height;
    float reloadTime;
    float currentReloadTime;
    std::vector<Bullet*> spawnedBullets;


};

