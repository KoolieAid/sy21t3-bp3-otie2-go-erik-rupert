#include "NormalBullet.h"
#include "Player.h"
#include "Scene.h"

NormalBullet::NormalBullet(int _x, int _y, float _activationTime)
	: PowerUp(_x, _y, _activationTime)
{

}

NormalBullet::~NormalBullet()
{
}

void NormalBullet::activate()
{
	// Checks if available to spawn bllet
	if (this->getCurrentActTime() != 0)
	{
		return;
	}


	this->setCurrentActTime(this->getActivationTime());

	SoundManager::playSound(this->getSoundChunk());
	Bullet* nBullet;
	nBullet = new Bullet((getSpawner()->getX() + getSpawner()->getWidth() / 2) - 13, getSpawner()->getY() - 4 + getSpawner()->getHeight() / 2, 0, -1, 10, Side::PLAYER_SIDE);
	
	getSpawner()->getSpawnedBulletsVec()->push_back(nBullet);
	this->getScene()->addGameObject(nBullet);

}

void NormalBullet::start()
{
	texture = loadTexture("gfx/playerBullet.png");
	SDL_QueryTexture(texture, NULL, NULL, getWidthReference(), getHeightReference());

	this->setSoundChunk(SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg"));
}

void NormalBullet::update()
{
	// Activation time
	if (this->getCurrentActTime() > 0) {
		this->setCurrentActTime(this->getCurrentActTime() - 1);
	}

	//move on game scene
	// might remove since it doesnt move in x axi
	this->setX(this->getX() + 0);
	if (getSpawner() == NULL)
	{
		this->setY(this->getY() + 5 * this->getSpeed());
	}

}

void NormalBullet::draw()
{
	//drawing on game scene

	// check if there is superplayer if not no draw
}
