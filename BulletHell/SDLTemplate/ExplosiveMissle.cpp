#include "ExplosiveMissle.h"
#include "GameScene.h"
#include "Explosion.h"
#include "ExplosiveMisslePowerUp.h"

ExplosiveMissle::ExplosiveMissle()
{

}

void ExplosiveMissle::start()
{
	texture = loadTexture("gfx/explosiveMissle.png");
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

}

void ExplosiveMissle::update()
{
	// collisions
	std::vector<Enemy*>* enemyVector = ((GameScene*)(this->getScene()))->getEnemyVector();
	for (int i = 0; i < enemyVector->size(); i++)
	{
		Enemy* enemy = enemyVector->at(i);

		int collision = checkCollision(this->getX(), this->getY(), this->getWidth(), this->getHeight(),
			enemy->getX(), enemy->getY(), enemy->getWidth(), enemy->getHeight());

		if (collision != 1) continue;

		//spawn an explosion

		std::cout << "EXPLOSIOOOONNN " << std::endl;

		Explosion* explosion = new Explosion();
		explosion->setX(x - width / 2);
		explosion->setY(y);
		manager->getSpawnedExplosions()->push_back(explosion);

		getScene()->addGameObject(explosion);
	}


	// copied from bullet
	x += (0);
	y += -1 * 5;
}

void ExplosiveMissle::draw()
{
	blit(texture, x, y, -90.0f);
}

int ExplosiveMissle::getX()
{
	return x;
}

int ExplosiveMissle::getY()
{
	return x;
}

void ExplosiveMissle::setX(int _x)
{
	x = _x;
}

void ExplosiveMissle::setY(int _y)
{
	y = _y;
}

int ExplosiveMissle::getWidth()
{
	return width;
}

int ExplosiveMissle::getHeight()
{
	return height;
}

void ExplosiveMissle::setManager(ExplosiveMisslePowerUp* man)
{
	manager = man;
}
