#include "Player.h"
#include "Scene.h"
#include "NormalBullet.h"
//#include "ExplosiveMisslePowerUp.h"

Player::~Player()
{
	for (int i = 0; i < spawnedBullets.size(); i++)
	{
		delete spawnedBullets[i];
	}
	spawnedBullets.clear();
}

void Player::start()
{
	texture = loadTexture("gfx/player.png");

	x = SCREEN_HEIGHT / 5;
	y = SCREEN_HEIGHT / 2;
	//reloadTime = 10;
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");

	lifeState = true;

	// PUT SECONDS HERE
	int powerTimeSeconds = 5;

	powerTime = powerTimeSeconds * 60;
	currentPowerTime = powerTime;

	displayedPowerTime = powerTime / 60;

	//currentPowerUp = new ExplosiveMisslePowerUp(-100, -100, 20);
	currentPowerUp = new NormalBullet(-100, -100, 10);
	currentPowerUp->setSpawner(this);
	getScene()->addGameObject(currentPowerUp);
}

void Player::update()
{
	for (int i = 0; i < spawnedBullets.size(); i++)
	{
		if (spawnedBullets[i]->getX() < 0 || spawnedBullets[i]->getX() > SCREEN_WIDTH ||
			spawnedBullets[i]->getY() < 0 || spawnedBullets[i]->getY() > SCREEN_HEIGHT)
		{
			Bullet* toDelete = spawnedBullets[i];
			spawnedBullets.erase(spawnedBullets.begin() + i);
			delete toDelete;
	
			break;
		}
	}

	if (!lifeState) return;
	SDL_GetMouseState(&x, &y);
	// Use if (SDL_SCANCODE_Whatever) if you want to use keyboard
	
	//SDL_Event event;
	//SDL_PollEvent(&event);
	//	std::cout << event.type << std::endl;
	//
	//while (SDL_PollEvent(&event)) {
	//	switch (event.type)
	//	{
	//	case SDL_MOUSEBUTTONDOWN:
	//		SoundManager::playSound(sound);
	//		break;
	//
	//	case 1024:
	//		SoundManager::playSound(sound);
	//	default:
	//		std::cout << event.type << std::endl;
	//		break;
	//	}
	//}

	if (app.keyboard[SDL_SCANCODE_F])
	{
		currentPowerUp->activate();
	}

	NormalBullet* nPower = dynamic_cast<NormalBullet*>(currentPowerUp);
	
	if (currentPowerTime > 0 && nPower == NULL)
	{
		currentPowerTime--;
	}
	
	if (currentPowerTime == 0)
	{
		NormalBullet* replacement = new NormalBullet(-100, -100, 10);
		this->changePowerUp(replacement);
		getScene()->addGameObject(replacement);
		currentPowerTime = powerTime;
	}
}

void Player::draw()
{
	if (lifeState)
		blit(texture, x, y, -90.0f);
}

float Player::getX()
{
	return x;
}

float Player::getY()
{
	return y;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height;
}

bool Player::isAlive()
{
	return lifeState;
}

void Player::kill()
{
	lifeState = false;
}

std::vector<Bullet*>* Player::getSpawnedBulletsVec()
{
	return &spawnedBullets;
}

void Player::changePowerUp(PowerUp* newPowerUp)
{
	currentPowerUp->setActive(false);
	currentPowerUp->setSpawner(NULL);
	currentPowerUp = newPowerUp;
	currentPowerUp->setSpawner(this);

	currentPowerTime = powerTime;
}

int Player::getDisplayedPowerTime()
{
	NormalBullet* nPower = dynamic_cast<NormalBullet*>(currentPowerUp);

	if (nPower != NULL)		return 0;

	if (currentPowerTime % 60 == 0)
		displayedPowerTime = currentPowerTime / 60;

	return displayedPowerTime;
}