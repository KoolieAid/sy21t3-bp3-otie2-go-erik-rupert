#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class ExplosiveMisslePowerUp;

class ExplosiveMissle final : public GameObject
{
public:
	ExplosiveMissle();

	void start() override;
	void update() override;
	void draw() override;

	int getX();
	int getY();

	void setX(int _x);
	void setY(int _y);

	int getWidth();
	int getHeight();

	void setManager(ExplosiveMisslePowerUp* man);

private:
	SDL_Texture* texture;
	int x;
	int y;

	int width;
	int height;

	ExplosiveMisslePowerUp* manager;
};