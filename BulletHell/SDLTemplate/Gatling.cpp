#include "Gatling.h"
#include "Player.h"
#include "Scene.h"

Gatling::Gatling(int _x, int _y, float _activationTime)
	: PowerUp(_x, _y, _activationTime)
{

}

Gatling::~Gatling()
{

}

void Gatling::activate()
{
	if (this->getCurrentActTime() != 0)
	{
		return;
	}
	this->setCurrentActTime(this->getActivationTime());

	// Remember to set sound
	this->getSoundChunk()->volume = 10;
	SoundManager::playSound(this->getSoundChunk());
	//Start of activate logic

	// USE LOOPS TO MAKE IT MORE EFFICIENT, BUT IM RUNNING OUT OF TIME TO PASS THIS; TOMORROW DEADLINE
	Bullet* bullet1 = new Bullet(((getSpawner()->getX() + getSpawner()->getWidth() / 2) - 5) - 13, getSpawner()->getY() - 4 + getSpawner()->getHeight() / 2, 0, -1, 20, Side::PLAYER_SIDE);

	getSpawner()->getSpawnedBulletsVec()->push_back(bullet1);
	this->getScene()->addGameObject(bullet1);

	Bullet* bullet2 = new Bullet(((getSpawner()->getX() + getSpawner()->getWidth() / 2) + 5) - 13, getSpawner()->getY() - 4 + getSpawner()->getHeight() / 2, 0, -1, 20, Side::PLAYER_SIDE);

	getSpawner()->getSpawnedBulletsVec()->push_back(bullet2);
	this->getScene()->addGameObject(bullet2);
}

void Gatling::start()
{
	texture = loadTexture("gfx/gatlingLogo.png");
	SDL_QueryTexture(texture, NULL, NULL, getWidthReference(), getHeightReference());

	this->setSoundChunk(SoundManager::loadSound("sound/warthog.ogg"));
}

void Gatling::update()
{
	// Activation time
	if (this->getCurrentActTime() > 0) {
		this->setCurrentActTime(this->getCurrentActTime() - 1);
	}

	// might remove since it doesnt move in x axi
	this->setX(this->getX() + 0);
	if (getSpawner() == NULL)
	{
		this->setY(this->getY() + 1 * this->getSpeed());
	}

	// Extra Logic
}

void Gatling::draw()
{
	if (getSpawner() == NULL)
	{
		blit(texture, getX(), getY());
	}
}
