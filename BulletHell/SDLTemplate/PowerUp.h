#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>
#include "util.h"

class Player;

class PowerUp : public GameObject
{
public:
    PowerUp(int _x, int _y, float _activationTime);
    virtual ~PowerUp();

    virtual void activate() = 0;

    float getActivationTime();

    void setActivationTime(float _time);

    int getX();
    int getY();
    void setX(int _x);
    void setY(int _y);

    float getSpeed();
    void setSpeed(float _s);

    void setWidth(int _nWidth);
    void setHeight(int _nHeight);

    int getCenterPosition();

    int getWidth();
    int getHeight();

    float getCurrentActTime();
    void setCurrentActTime(float _newActTime);

    Mix_Chunk* getSoundChunk();
    void setSoundChunk(Mix_Chunk* _sound);

    Player* getSpawner();
    void setSpawner(Player* player);

    bool isActive();
    void setActive(bool state);
protected:
    SDL_Texture* texture;

    int* getWidthReference();
    int* getHeightReference();

private:
    float activationTime;
    float currentActivationTime;

    Mix_Chunk* sound;

    int x;
    int y;
    int width;
    int height;
    float speed;

    Player* superPlayer;

    bool active;
};

