#pragma once
#include "PowerUp.h"
#include "Lightning.h"


class LightningPowerUp final : public PowerUp
{
public :
	LightningPowerUp(int _x, int _y, float _activationTime);
	~LightningPowerUp() override;

	void activate() override;

	void start() override;
	void update() override;
	void draw() override;

private:
	Lightning* lightningObj;
};

