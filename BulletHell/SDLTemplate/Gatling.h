#pragma once
#include "PowerUp.h"
#include "Bullet.h"

class Gatling final : public PowerUp
{
public:
	Gatling(int _x, int _y, float _activationTime);
    ~Gatling() override;

    void activate() override;

    void start() override;
    void update() override;
    void draw() override;
};