#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>

class PowerUp;

class Player :
    public GameObject
{
public:
    ~Player();
    void start() override;
    void update() override;
    void draw() override;

    float getX();
    float getY();

    int getWidth();
    int getHeight();

    bool isAlive();
    void kill();

    std::vector<Bullet*>* getSpawnedBulletsVec();

    void changePowerUp(PowerUp* newPowerUp);

    int getDisplayedPowerTime();

private:
    SDL_Texture* texture;
    Mix_Chunk* sound;
    int x;
    int y;
    int width;
    int height;
    //float reloadTime;
    //float currentReloadTime;
    std::vector<Bullet*> spawnedBullets;
    bool lifeState;

    PowerUp* currentPowerUp;

    int powerTime;
    int currentPowerTime;

    int displayedPowerTime;
};

