#pragma once
#include "GameObject.h"
#include "GameScene.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include <vector>
#include "Missile.h"

// unsure if this will make circular dependency
#include "Player.h"

class Boss final : public GameObject
{
public:
	Boss();
	~Boss();
	void start() override;
	void update() override;
	void draw() override;

	int getX();
	int getY();

	void setX(int _x);
	void setY(int _y);

	int getWidth();
	int getHeight();

	void setTarget(Player* player);

	bool hasStartedRoutine;

	int getHp();
	void setHp(int _hp);
private:

	std::vector<Missile*> spawnedMissles;
	std::vector<Bullet*> spawnedBullets;

	int x;
	int y;

	int width;
	int height;
	int directionX;
	void spawnMissle(int offset);

	void spawnMissleArray();

	Player* target;

	int hp;

	SDL_Texture* texture;
	SDL_Texture* hpBarTexture;
	Mix_Chunk* bulletSound;
	Mix_Chunk* missleSound;

	int missleArrayInnerSpawningTimer;
	int currentMissleArrayInnerSpawningTimer;

	int missleArrayOuterTimer;
	int currentMissleArrayOuterTimer;

	int globalFramesSpent;
	bool isFiringMissles;
	int arraysFired;

	int currentReloadTime;
	int reloadTime;
};