#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class Player;

class Missile final : public GameObject
{
public:
	Missile();

	virtual void start() override;
	virtual void update() override;
	virtual void draw() override;

	void setTarget(Player* _player);
	Player* getTarget(); // im not sure if im gonna use this or not

	float getX();
	float getY();

	void setX(float _x);
	void setY(float _y);

	int getWidth();
	int getHeight();

private:
	SDL_Texture* texture;

	Player* target;

	float x;
	float y;
	float directionX;
	float directionY;

	int width;
	int height;

	float angle;

	void collisionLogic();
};

