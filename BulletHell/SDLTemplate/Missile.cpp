#include "Missile.h"
#include "util.h"
#include "Player.h"

Missile::Missile()
{

}

void Missile::start()
{
	//Texture to be added
	texture = loadTexture("gfx/homingMissle.png");
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Missile::update()
{
	// Collision checks here and rotation and direction computation goes here

	// Movement
	float dx = -1;
	float dy = 0;
	
	calcSlope(target->getX() + target->getWidth() / 2, target->getY() + target->getHeight() / 2, x, y, &dx, &dy);
	
	x += (dx * 5);
	y += (dy * 5);

	// Rotation
	float xAxis;
	float yAxis;

	xAxis = target->getX() - x;
	yAxis = target->getY() - y;

	float inside = xAxis - yAxis;

	//angle = atan(yAxis / xAxis);
	angle = atan2(yAxis, xAxis);
	angle = angle * (180/M_PI);

	//if (xAxis < 0) angle += 180;

	// end of rotation logic

	collisionLogic();
}

void Missile::draw()
{
	//rememeber to add rotation
	blit(texture, x, y,angle );
}

void Missile::setTarget(Player* _player)
{
	target = _player;
}

Player* Missile::getTarget()
{
	return target;
}

float Missile::getX()
{
	return x;
}

float Missile::getY()
{
	return y;
}

void Missile::setX(float _x)
{
	x = _x;
}

void Missile::setY(float _y)
{
	y = _y;
}

int Missile::getWidth()
{
	return width;
}

int Missile::getHeight()
{
	return height;
}

void Missile::collisionLogic()
{
	int collision = checkCollision(this->getX(), this->getY(), this->getWidth(), this->getHeight(),
		target->getX(), target->getY(), target->getWidth(), target->getHeight());

	if (collision != 1) return;

	target->kill();
}
