#include "Boss.h"

Boss::Boss()
{

}

Boss::~Boss()
{
	// If Boss died, transfer all objects to garbage bin
	// bullets
	int storedSize;
	std::vector<GameObject*>* deleteVector = ((GameScene*)this->getScene())->getGarbageBin();
	storedSize = spawnedBullets.size();
	for (int i = 0; i < storedSize; i++)
	{
		Bullet* toDelete = spawnedBullets[0];
		deleteVector->push_back(toDelete);
		spawnedBullets.erase(spawnedBullets.begin());
	}

	// missles
	storedSize = spawnedMissles.size();
	for (int i = 0; i < storedSize; i++)
	{
		Missile* toDelete = spawnedMissles[0];
		deleteVector->push_back(toDelete);
		spawnedMissles.erase(spawnedMissles.begin());
	}
}

void Boss::start()
{
	// generic start
	texture = loadTexture("gfx/bossDan.png");
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	hpBarTexture = loadTexture("gfx/hpBar.png");

	// extra logic

	directionX = 1;
	hp = 10;

	// Missle Firerate
	missleArrayInnerSpawningTimer = 5 * 60;
	currentMissleArrayInnerSpawningTimer = missleArrayInnerSpawningTimer;

	// When to spawn array
	missleArrayOuterTimer = 10 * 60;
	currentMissleArrayOuterTimer = missleArrayOuterTimer;

	globalFramesSpent = 0;

	isFiringMissles = false;
	arraysFired;
	reloadTime = 20;
	currentReloadTime = 0;

	//Sound
	bulletSound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	bulletSound->volume = 60;

	hasStartedRoutine = false;
}

void Boss::update()
{
	if (!hasStartedRoutine) return;
	// Missle
	if (currentMissleArrayInnerSpawningTimer > 0) currentMissleArrayInnerSpawningTimer--;

	if (currentMissleArrayInnerSpawningTimer == 0)
	{
		spawnMissleArray();
		currentMissleArrayInnerSpawningTimer = missleArrayInnerSpawningTimer;
	}

	// Normal bullets copied from enemy cpp

	if (currentReloadTime > 0) {
		currentReloadTime--;
	}

	if (currentReloadTime == 0) {

		float dx = -1;
		float dy = 0;

		calcSlope(target->getX(), target->getY(), x, y, &dx, &dy);

		SoundManager::playSound(bulletSound);
		Bullet* bulletLeft = new Bullet(x + 35, y + height - 10, dx, dy, 10, Side::ENEMY_SIDE);
		Bullet* bulletRight = new Bullet(x + width - 35, y + height - 10, dx, dy, 10, Side::ENEMY_SIDE);

		spawnedBullets.push_back(bulletLeft);
		getScene()->addGameObject(bulletLeft);

		spawnedBullets.push_back(bulletRight);
		getScene()->addGameObject(bulletRight);

		currentReloadTime = reloadTime;
	}

	// If bullets are out of bound, transfer to garbage collection in game scene
	// yes, i had a garbage collection since milestone 4 just named differently
	std::vector<GameObject*>* deleteVector = ((GameScene*)this->getScene())->getGarbageBin();
	for (int i = 0; i < spawnedBullets.size(); i++)
	{
		// If out of bounds
		if (spawnedBullets[i]->getX() < 0 || spawnedBullets[i]->getX() > SCREEN_WIDTH ||
			spawnedBullets[i]->getY() < 0 || spawnedBullets[i]->getY() > SCREEN_HEIGHT)
		{
			Bullet* toDelete = spawnedBullets[i];
			deleteVector->push_back(toDelete);
			spawnedBullets.erase(spawnedBullets.begin() + i);
			//delete toDelete;

			break;
		}
	}

	// no transfering of missles to garbage, bc it doesnt really go off screen since the player is limited by mouse range

	globalFramesSpent++;
	//movement

	x += directionX * 2;
	if (x + width > SCREEN_WIDTH || x <= 0){
		directionX *= -1;
	}
}

void Boss::draw()
{
	blit(texture, x, y);

	drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT - 100, 255, 0, 0, TEXT_CENTER, "HP: %01d", hp);
}

int Boss::getX()
{
	return x;
}

int Boss::getY()
{
	return y;
}

void Boss::setX(int _x)
{
	x = _x;
}

void Boss::setY(int _y)
{
	y = _y;
}

int Boss::getWidth()
{
	return width;
}

int Boss::getHeight()
{
	return height;
}

void Boss::setTarget(Player* player)
{
	target = player;
}

int Boss::getHp()
{
	return hp;
}

void Boss::setHp(int _hp)
{
	hp = _hp;
}

void Boss::spawnMissle(int offset)
{
	// Test Missles logic
	Missile* missle = new Missile();
	missle->setTarget(target);
	missle->setX(x + getWidth() / 2 + offset);
	missle->setY(y + getHeight());
	getScene()->addGameObject(missle);
	spawnedMissles.push_back(missle);
}

void Boss::spawnMissleArray()
{
	for (int i = -60; i <= 60; i += 30)
	{
		spawnMissle(i);
	}
}