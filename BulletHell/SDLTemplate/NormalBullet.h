#pragma once
#include "PowerUp.h"
#include "Bullet.h"

class NormalBullet final : public PowerUp
{
public:

    NormalBullet(int _x, int _y, float _activationTime);
    ~NormalBullet() override;

    void activate() override;

    void start() override;
    void update() override;
    void draw() override;

private:

};

