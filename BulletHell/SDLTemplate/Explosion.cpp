#include "Explosion.h"
#include <vector>
#include "Enemy.h"
#include "Boss.h"

Explosion::Explosion()
{
}

void Explosion::start()
{
	texture = loadTexture("gfx/explosion.png");
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Explosion::update()
{
	std::vector<GameObject*>* deleteVector = ((GameScene*)this->getScene())->getGarbageBin();
	std::vector<Enemy*>* enemyVector = ((GameScene*)this->getScene())->getEnemyVector();

	for (int i = 0; i < enemyVector->size(); i++)
	{
		// enemyVector[i] doesnt work lmao; dont think u can outsmart me
		Enemy* enemy = enemyVector->at(i);

		int collision = checkCollision(this->getX(), this->getY(), this->getWidth(), this->getHeight(),
			enemy->getX(), enemy->getY(), enemy->getWidth(), enemy->getHeight());

		if (collision != 1) continue;

		std::cout << "collided" << std::endl;
		deleteVector->push_back(enemy);
		enemyVector->erase(enemyVector->begin() + i);

		// i need to debug this just in case
		//deleteVector->push_back(this);

		break;
	}
}

void Explosion::draw()
{
	blit(texture, x, y);
}

int Explosion::getX()
{
	return x;
}

int Explosion::getY()
{
	return x;
}

void Explosion::setX(int _x)
{
	x = _x;
}

void Explosion::setY(int _y)
{
	y = _y;
}

int Explosion::getWidth()
{
	return width;
}

int Explosion::getHeight()
{
	return height;
}
