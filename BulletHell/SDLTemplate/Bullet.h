#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

enum class Side
{
    PLAYER_SIDE,
    ENEMY_SIDE
};

class Player;

class Bullet :
    public GameObject
{
public:

    Bullet(int _x, int _y, float _directionX, float _directionY, int _speed, Player* _target);

    Bullet(int _x, int _y, float _directionX, float _directionY, int _speed, Side _side);
    virtual void start() override;
    virtual void update() override;
    virtual void draw() override;

    float getX();
    float getY();
    int getWidth();
    int getHeight();

    Side getSide();

    int getDamage();
private:
    SDL_Texture* texture;
    Side side;
    float x;
    float y;
    int speed;
    int width;
    int height;
    float directionX;
    float directionY;
    float airResistance;


    Player* target;

    uint64_t n;
    uint64_t p;

    int damage;
};

