#include "Lightning.h"
#include "util.h"
#include "Enemy.h"
#include "GameScene.h"
#include "Boss.h"

Lightning::Lightning()
{

}

Lightning::~Lightning()
{
}

void Lightning::start()
{
	texture = loadTexture("gfx/lightning.png");
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	// Extra logic
	this->setX(-100);

	isFlipped = false;

	damage = 50;
}

void Lightning::update()
{
	// LMAO am i cheating??
	std::vector<GameObject*>* deleteVector = ((GameScene*)this->getScene())->getGarbageBin();
	std::vector<Enemy*>* enemyVector = ((GameScene*)this->getScene())->getEnemyVector();

	for (int i = 0; i < enemyVector->size(); i++)
	{
		// enemyVector[i] doesnt work lmao; dont think u can outsmart me
		Enemy* enemy = enemyVector->at(i);

		int collision = checkCollision(this->getX(), this->getY(), this->getWidth(), this->getHeight(),
			enemy->getX(), enemy->getY(), enemy->getWidth(), enemy->getHeight());

		if (collision != 1) continue;

		deleteVector->push_back(enemy);
		enemyVector->erase(enemyVector->begin() + i);

		((GameScene*)this->getScene())->addPoint();

		break;
	}

	// flipping off lmao
	isFlipped = !isFlipped;

	// boss logic
	std::vector<GameObject*>* objects = ((GameScene*)this->getScene())->getObjectsVector();

	for (int i = 0; i < objects->size(); i++)
	{
		Boss* tBoss = dynamic_cast<Boss*>(objects->at(i));

		if (tBoss == NULL) continue;

		int collision = checkCollision(this->getX(), this->getY(), this->getWidth(), this->getHeight(),
			tBoss->getX(), tBoss->getY(), tBoss->getWidth(), tBoss->getHeight());

		if (collision == 1)
		{
			tBoss->setHp(tBoss->getHp() - damage);
		}

	}

}

void Lightning::draw()
{
	if (app.keyboard[SDL_SCANCODE_F])
	{
		if (isFlipped)
		{
			blit(texture, x, y, 180);
		}
		else {
			blit(texture, x, y);
		}
	}
}

int Lightning::getX()
{
	return x;
}

int Lightning::getY()
{
	return y;
}

void Lightning::setX(int _x)
{
	x = _x;
}

void Lightning::setY(int _y)
{
	y = _y;
}

int Lightning::getWidth()
{
	return width;
}

int Lightning::getHeight()
{
	return height;
}
