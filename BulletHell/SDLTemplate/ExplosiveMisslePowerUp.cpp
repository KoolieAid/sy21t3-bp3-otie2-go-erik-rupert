#include "ExplosiveMisslePowerUp.h"
#include "ExplosiveMissle.h"
#include "Player.h"
#include "GameScene.h"

ExplosiveMisslePowerUp::ExplosiveMisslePowerUp(int _x, int _y, float _activationTime)
	:	PowerUp(_x, _y, _activationTime)
{

}

ExplosiveMisslePowerUp::~ExplosiveMisslePowerUp()
{
}

void ExplosiveMisslePowerUp::activate()
{
	if (this->getCurrentActTime() != 0) return;
	this->setCurrentActTime(this->getActivationTime());

	// Remember to set sound
	//this->getSoundChunk()->volume = 10;
	//SoundManager::playSound(this->getSoundChunk());

	//Start of activate logic
	ExplosiveMissle* missle = new ExplosiveMissle();
	missle->setX(getSpawner()->getX());
	missle->setY(getSpawner()->getY());
	missle->setManager(this);
	spawnedMissles.push_back(missle);

	this->getScene()->addGameObject(missle);
}

void ExplosiveMisslePowerUp::start()
{
	texture = loadTexture("gfx/gatlingLogo.png");
	SDL_QueryTexture(texture, NULL, NULL, getWidthReference(), getHeightReference());

	// dont forget to set sound
	//this->setSoundChunk(SoundManager::loadSound("sound/warthog.ogg"));
}

void ExplosiveMisslePowerUp::update()
{
	// Activation time
	if (this->getCurrentActTime() > 0) {
		this->setCurrentActTime(this->getCurrentActTime() - 1);
	}

	// might remove since it doesnt move in x axi
	this->setX(this->getX() + 0);
	if (getSpawner() == NULL)
	{
		this->setY(this->getY() + 1 * this->getSpeed());
	}

	// extra logic
	


	//delete spawned missles


	// delete spawned explosions
	for (int i = 0; i < spawnedExplosions.size(); i++)
	{
		Explosion* toDelete = spawnedExplosions[i];
		spawnedExplosions.erase(spawnedExplosions.begin() + i);
		delete toDelete;
	}
}

void ExplosiveMisslePowerUp::draw()
{
	if (getSpawner() == NULL)
	{
		blit(texture, getX(), getY());
	}
}

std::vector<ExplosiveMissle*>* ExplosiveMisslePowerUp::getSpawnedMissles()
{
	return &spawnedMissles;
}

std::vector<Explosion*>* ExplosiveMisslePowerUp::getSpawnedExplosions()
{
	return &spawnedExplosions;
}
