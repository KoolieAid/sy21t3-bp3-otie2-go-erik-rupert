#pragma once
#include "PowerUp.h"
#include <vector>
#include "ExplosiveMissle.h"
#include "Explosion.h"
// Includde a explosive missle

class ExplosiveMisslePowerUp final : public PowerUp
{
public:
	ExplosiveMisslePowerUp(int _x, int _y, float _activationTime);
	~ExplosiveMisslePowerUp() override;

	void activate() override;

	void start() override;
	void update() override;
	void draw() override;

	std::vector<ExplosiveMissle*>* getSpawnedMissles();
	std::vector<Explosion*>* getSpawnedExplosions();

private:
	std::vector<ExplosiveMissle*> spawnedMissles;
	std::vector<Explosion*> spawnedExplosions;
};

