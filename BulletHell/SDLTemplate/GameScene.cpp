#include "GameScene.h"
#include "Boss.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player;
	addGameObject(player);

	points = 0;

	currentSpawnTimer = 0;
	spawnTime = 300;
	
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
}

void GameScene::draw()
{
	Scene::draw();

	drawText(115, 30, 255, 255, 255, TEXT_CENTER, "POINTS: %02d", points);
	drawText(580, 30, 255, 255, 255, TEXT_CENTER, "POWER LEFT: %01d", ((Player*)player)->getDisplayedPowerTime());

	if (!((Player*)player)->isAlive())
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "GAME OVER");
	}
}

void GameScene::update()
{
	Scene::update();

	if (points % 25 == 0 && points != 0 || boss != NULL)
	{
		spawnBoss();
	}
	else
	{
		spawnTimer();
	}

	collisionChecker();
	outOfBoundsChecker();

	spawnRandomPowerUp();
	powerUpOutOfBoundsChecker();
	powerUpCollisionChecker();

	bossHpChecker();
	throwGarbageAway();
}

std::vector<GameObject*>* GameScene::getGarbageBin()
{
	return &garbageBin;
}

std::vector<Enemy*>* GameScene::getEnemyVector()
{
	return &spawnedEnemies;
}

std::vector<GameObject*>* GameScene::getObjectsVector()
{
	return &objects;
}

Boss* GameScene::getBoss()
{
	return boss;
}

void GameScene::addPoint()
{
	points++;
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy;
	this->addGameObject(enemy);
	enemy->setTarget((Player*)player);

	enemy->setPosition(rand() % ((SCREEN_WIDTH - enemy->getWidth()) - 1 + 1) + 1, 1);
	spawnedEnemies.push_back(enemy);
}

void GameScene::despawn(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}

	if (index != -1)
	{
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::spawnTimer()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;

	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			spawn();

		}
		currentSpawnTimer = spawnTime;
	}
}

void GameScene::collisionChecker()
{
	//Collision
	for (int i = 0; i < objects.size(); i++)
	{
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		if (bullet != NULL)
		{
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				Player* _player = (Player*)player;
				int collision = checkCollision(_player->getX(), _player->getY(), _player->getWidth(), _player->getHeight(),
					bullet->getX(), bullet->getY(), bullet->getWidth(), bullet->getHeight());


				if (collision == 1)
				{
					_player->kill();
					break;
				}
			}
			else if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];
					int collision = checkCollision(currentEnemy->getX(), currentEnemy->getY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getX(), bullet->getY(), bullet->getWidth(), bullet->getHeight());

					if (collision == 1)
					{
						despawn(currentEnemy);
						points++;
						break;
					}
				}

				if (boss != NULL)
				{
					int collision = checkCollision(boss->getX(), boss->getY(), boss->getWidth(), boss->getHeight(),
						bullet->getX(), bullet->getY(), bullet->getWidth(), bullet->getHeight());

					if (collision == 1)
					{
						boss->setHp(boss->getHp() - bullet->getDamage());
					}
				}

			}
		}
	}
}

void GameScene::outOfBoundsChecker()
{
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		Enemy* enemy = spawnedEnemies[i];
		
		if (enemy->getX() < 0 || enemy->getX() > SCREEN_WIDTH ||
			enemy->getY() < 0 || enemy->getY() > SCREEN_HEIGHT)
		{
			//despawn(enemy);
			spawnedEnemies.erase(spawnedEnemies.begin() + i);
			garbageBin.push_back(enemy);
			break;
		}
	}
}

void GameScene::spawnRandomPowerUp()
{
	int randReference = rand() % ((200 - 1) + 1) + 1;

	if (randReference <= 1)
	{
		PowerUp* powUp = nullptr;
		randReference = rand() % ((100 - 1) + 1) + 1;
		if (randReference <= 50)
		{
			powUp = new Gatling(rand() % ((SCREEN_WIDTH) - 1 + 1) + 1, 1, 2);
		}
		else
		{
			powUp = new LightningPowerUp(rand() % ((SCREEN_WIDTH)-1 + 1) + 1, 1, 2);
		}

		this->spawnedPowerUps.push_back(powUp);
		this->addGameObject(powUp);
	}
}

void GameScene::powerUpOutOfBoundsChecker()
{
	for (int i = 0; i < spawnedPowerUps.size(); i++)
	{
		PowerUp* powerUp = spawnedPowerUps[i];

		if (powerUp->getY() < 0 || powerUp->getY() > SCREEN_HEIGHT &&
			powerUp->getSpawner() == NULL)
		{
			// Depsawn the power up im running out of time lol only a few hours left
			int index = -1;
			for (int i = 0; i < spawnedPowerUps.size(); i++)
			{
				if (powerUp == spawnedPowerUps[i])
				{
					index = i;
					break;
				}
			}

			if (index != -1)
			{
				garbageBin.push_back(spawnedPowerUps[i]);
				spawnedPowerUps.erase(spawnedPowerUps.begin() + index);
			}
			// end of despawn

			break;
		}

	}
}

void GameScene::spawnBoss()
{
	if (boss == NULL)
	{
		boss = new Boss();
		boss->setX(300);
		boss->setY(-200);
		boss->setTarget((Player*)player);
		this->addGameObject(boss);
	}

	if (boss->getY() < 100)
	{
		boss->setY(boss->getY() + 1);
		boss->setHp(boss->getHp() + 100);
	}
	if (boss->getY() == 100 && boss->hasStartedRoutine == false)
	{
		boss->hasStartedRoutine = true;
	}
}

void GameScene::powerUpCollisionChecker()
{
	for (int i = 0; i < objects.size(); i++)
	{
		PowerUp* powerUp = dynamic_cast<PowerUp*>(objects[i]);

		if (powerUp != NULL)
		{
			Player* _player = (Player*)player;
			int collision = checkCollision(_player->getX(), _player->getY(), _player->getWidth(), _player->getHeight(),
				powerUp->getX(), powerUp->getY(), powerUp->getWidth(), powerUp->getHeight());

			if (collision == 1)
			{
				powerUp->setActive(true);
				powerUp->setX(-100);
				_player->changePowerUp(powerUp);
				break;
			}
			
		}
	}
}

void GameScene::throwGarbageAway()
{
	for (int i = 0; i < garbageBin.size(); i++)
	{
		GameObject* toDelete = garbageBin[i];
	
		garbageBin.erase(garbageBin.begin() + i);
		delete toDelete;
	
		break;
	}
}

void GameScene::bossHpChecker()
{
	if (boss == nullptr) return;

	if (boss->getHp() <= 0)
	{
		boss->hasStartedRoutine = false;
		garbageBin.push_back(boss);
		points += rand() % 75 + 25;
		boss = nullptr;
	}
}