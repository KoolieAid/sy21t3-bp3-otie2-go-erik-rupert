#include "PowerUp.h"
#include "Player.h"

PowerUp::PowerUp(int _x, int _y, float _activationTime)
{
	x = _x;
	y = _y;

	activationTime = _activationTime;

	speed = rand() % ((6 - 3) + 1) + 3;

	active = false;
}

PowerUp::~PowerUp()
{
}

float PowerUp::getActivationTime()
{
	return activationTime;
}

void PowerUp::setActivationTime(float _time)
{
	this->activationTime = _time;
}

int PowerUp::getX()
{
	return x;
}

int PowerUp::getY()
{
	return y;
}

void PowerUp::setX(int _x)
{
	x = _x;
}

void PowerUp::setY(int _y)
{
	y = _y;
}

float PowerUp::getSpeed()
{
	return speed;
}

void PowerUp::setSpeed(float _s)
{
	speed = _s;
}

void PowerUp::setWidth(int _nWidth)
{
	width = _nWidth;
}

void PowerUp::setHeight(int _nHeight)
{
	height = _nHeight;
}

int PowerUp::getCenterPosition()
{
	int _x = getX() + (getWidth() / 2);
	int _y = getY() + (getHeight() / 2);
	return _x + _y;
}

int PowerUp::getWidth()
{
	return width;
}

int PowerUp::getHeight()
{
	return height;
}

float PowerUp::getCurrentActTime()
{
	return currentActivationTime;
}

void PowerUp::setCurrentActTime(float _newActTime)
{
	this->currentActivationTime = _newActTime;
}

Mix_Chunk* PowerUp::getSoundChunk()
{
	return sound;
}

void PowerUp::setSoundChunk(Mix_Chunk* _sound)
{
	sound = _sound;
}

Player* PowerUp::getSpawner()
{
	return superPlayer;
}

void PowerUp::setSpawner(Player* player)
{
	superPlayer = player;
}

bool PowerUp::isActive()
{
	return active;
}

void PowerUp::setActive(bool state)
{
	active = state;
}

int* PowerUp::getWidthReference()
{
	return &width;
}

int* PowerUp::getHeightReference()
{
	return &height;
}
